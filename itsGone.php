<?php
session_start();
include "top.php";
//This page pulls information on images selected from form.php it also conatians a comment section for each individual image
//query to select image info
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//  Connect to Database
//
$query = 'SELECT pmkImageID, fldFolder, fldImage ';
$query .= 'FROM tblImages ';
$query .= 'WHERE fldImage LIKE ?';
//initialize variables
$imageID = "";
//grab image selected
if (isset($_POST['selRemove'])) {
    $imageID = htmlentities($_POST['selRemove'], ENT_QUOTES, "UTF-8");
    $_SESSION['imageid'] = htmlentities($_POST['selRemove'], ENT_QUOTES, "UTF-8");
}
$imgToDelete = $thisDatabaseReader->select($query, array($imageID), 1, 0, 0, 0, false, false);
//%%%%%%%%%%%%%%%   DEBUG   %%%%%%%%%%%%%%%//
if (DEBUG) {
    print "<p>Contents of the array<pre>";
    print_r($records);
    print "</pre></p>";
}

if (is_array($imgToDelete)) {
    foreach ($imgToDelete as $infoPiece) {

        $forUnlinking = $infoPiece['fldFolder'].'/'.$infoPiece['fldImage'];
        
        print $infoPiece['fldImage'] . "<br>";
        $query = 'DELETE FROM tblImages WHERE pmkImageID="' . $infoPiece['pmkImageID'] . '"';
        $thisDatabaseAdmin->delete($query, "", 1, 0, 2, 0, false, false);
        unlink($forUnlinking);
    }
}  
//#########################################################################################################################################################################
require_once('lib/security.php');
include "lib/validation-functions.php";
//%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%
// DEBUG system setup
$debug = false;
if (isset($_GET["debug"])) { // ONLY do this in a classroom environment
    $debug = false;
}
if ($debug)
    print "<p>DEBUG MODE IS ON</p>";
//%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%git status
//
// define security variable

$yourURL = $domain . $phpSelf;

// create array to hold error messages filled (if any) in 2d displayed in 3c.
$errorMsg = array();
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//Process for when the form is submitted

if (isset($_POST["btnRemove"])) {
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// Security check

    if (!securityCheck(true)) {
        $msg = "<p>Sorry you cannot access this page. ";
        $msg .= "Security breach detected and reported</p>";
        die($msg);
    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// Process for when the form passes validation (the errorMsg array is empty)

    if (!$errorMsg) {
        if ($debug)
            print "<p>Form is valid</p>";
        if ($debug)
            print "<p>transaction complete ";
    } // end form is valid
} // ends if form was submitted.
//#############################################################################
//Display Form
?><?php
if (isset($_POST["btnRemove"])) {

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//
// Security
// 
    if (!securityCheck(true)) {
        $msg = "<p>Sorry you cannot access this page. ";
        $msg .= "Security breach detected and reported</p>";
        die($msg);
    }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// Process Form - Passed Validation
// Process for when the form passes validation (the errorMsg array is empty)
//
    if (!$errorMsg) {
        if ($debug)
            print "<p>Form is valid</p>";
    } // end form is valid
} // ends if form was submitted.
//#############################################################################
// SECTION 3 Display Form
//
?>

<article id="main">

<?php
//####################################
// If its the first time coming to the form or there are errors we are going
// to display the form.
if (isset($_POST["btnRemove"]) AND empty($errorMsg)) { // closing of if marked with: end body submit
} else {

//####################################
//  display any error messages before we print out the form

    if ($errorMsg) {
        print '<div id="errors">';
        print "<ol>\n";
        foreach ($errorMsg as $err) {
            print "<li>" . $err . "</li>\n";
        }
        print "</ol>\n";
        print '</div>';
    }

    //Setting up query to be able to access data
    $query = 'SELECT DISTINCT fldFolder, fldImage ';
    $query .= 'FROM tblImages ';
    $query .= 'ORDER BY fldFolder';

    //Reads data
    $imgPaths = $thisDatabaseReader->select($query, "", 0, 1, 0, 0, false, false);
    ?>
                    <i>

                    </i>        
                </select>
            </label>
            <input type="submit">
        </form>
        -->
    <?php
} // end body submit


print("</article>");
include "footer.php";
?><br><BR>
</body>
</html>


