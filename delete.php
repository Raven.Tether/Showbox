<?php
include 'top.php';
//%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^
// SECTION: 1 Initialize variables
// SECTION: 1a.
// variables for the classroom purposes to help find errors.

$debug = false;


if ($debug){
    print "<p>DEBUG MODE IS ON</p>";
}
    $yourURL = $domain . $phpSelf;
    $errorMsg = array();

// array used to hold form values
$dataRecord = array();
$data = array();

if (isset($_POST["btnSubmit"])) {
    if (!securityCheck(true)) {
        $msg = "<p>Sorry you cannot access this page. ";
        $msg .= "Security breach detected and reported</p>";
        die($msg);
    }
    if (!$errorMsg) {
        if ($debug)
            print "<p>Form is valid</p>";
    } // end form is valid
} // ends if form was submitted.
?>

<article id="main">

    <?php
//####################################
// If its the first time coming to the form or there are errors we are going
// to display the form.
    if (isset($_POST["btnSubmit"]) AND empty($errorMsg)) { // closing of if marked with: end body submit
    } else {
//####################################
//  display any error messages before we print out the form
        if ($errorMsg) {
            print '<div id="errors">';
            print "<ol>\n";
            foreach ($errorMsg as $err) {
                print "<li>" . $err . "</li>\n";
            }
            print "</ol>\n";
            print '</div>';
        }
        
        
        //Setting up query to be able to access 
        $query = 'SELECT DISTINCT fldFolder ';
        $query .= 'FROM tblImages ';
        $query .= 'ORDER BY fldFolder';
        //Reads data
        $imgPaths = $thisDatabaseReader->select($query, "", 0, 1, 0, 0, false, false);
        ?>
        <!-- Form that allows the user to select which image they want to view and sends the data to the POST array -->
        <form action="removeIt.php"
              method="post"
              id="frmRegister">

            <h3>Select a Folder</h3>
            <label for="selFolder" class="required">
                <select name="selFolder" id="selFolder" tabindex="130">
                    <?php
                    //Foreach loop to iterate between all the images
                    foreach ($imgPaths as $imgPath) {
                        print'<option value="' . $imgPath['fldFolder'] . '">' . $imgPath['fldFolder'] . '</option>';
                    }
                    ?></select>
            </label>
            <input type="submit" id="btnSubmit">
        </form>

        <?php
    } // end body submit
    ?>
</article>

<?php include "footer.php"; ?>

</body>
</html>
