<?php
include 'top.php';
//uses bootstap
//
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//  Image Connect to database
//
//Created a query that grabs the  data 
$query = 'SELECT fldFolder, fldImage ';
$query .= 'FROM tblImages ';
$query .= 'ORDER BY fldFolder';

//Reads data
$imgData = $thisDatabaseReader->select($query, "", 0, 1, 0, 0, false, false);

//Created a table to display the information pulled from the database

print"<article>";
print'<div class="gallery imgs">
  ';

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//  Image Print Info
//
//If statement that checks if there's data in the array
if (is_array($imgData)) {
    //Foreach loop that pulls the correct data from the database into the table
    foreach ($imgData as $imgInfo) {
        print"
        <div>    
        <img src=" . $imgInfo['fldFolder'] . "/" . $imgInfo['fldImage'] . " alt='" . $imgInfo['fldImage'] . "' />
        <p>
        Folder:" . $imgInfo['fldFolder'] . "
        </p>
        <p>
        File Name: " . $imgInfo['fldImage'] . "
        </p>
        </div>
        <br><br>

        ";
    }
}

print("</div></article>");
include "footer.php";
?><br><BR>
</body>
</html>