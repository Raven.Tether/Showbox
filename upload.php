<?php
//
//      Upload the Images
//
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//  Includes
//
include 'top.php';
$yourURL = $domain . $phpSelf;
require_once('lib/security.php');
include "lib/validation-functions.php";


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//    Initialize Variables
//
$folder_upload_to = "";
$folderERROR = false;
$errors = array();


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//      Error Handling
//
if (isset($_POST['folderTo'])) {
    $folder_upload_to = htmlentities($_POST["folderTo"], ENT_QUOTES, "UTF-8");
    if ($folder_upload_to == "") {
        $errors[] = "Please enter your folder name";
        $folderERROR = true;
    } elseif (!verifyAlphaNum($folder_upload_to)) {
        $errors[] = "Your folder name appears to have extra character.";
        $folderERROR = true;
    }

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//  Image Data
//
if (isset($_FILES['image'])) {

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//  Image Initialize Variables 
//    
    $file_name = $_FILES['image']['name'];
    $file_size = $_FILES['image']['size'];
    $file_tmp = $_FILES['image']['tmp_name'];
    $file_type = $_FILES['image']['type'];
    $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));

    
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//  Image Error Checking
//    
    $expensions = array("jpeg", "jpg", "png");

    if (in_array($file_ext, $expensions) === false) {
        $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
    }

    if ($file_size > 2097152) {
        $errors[] = 'File size must be excately 2 MB';
    }

    if (empty($errors) == true) {

        //$folder_upload_to = ;

        if (!file_exists($folder_upload_to)) {
            mkdir($folder_upload_to, 0777, true);
        }


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//  Image Congratulatory Message
//        
        move_uploaded_file($file_tmp, $folder_upload_to . "/" . $file_name);
        $dataGo = array($folder_upload_to, $file_name);
        echo "<br><br><p>Congratulations, you have successfully "
        . "uploaded your photo to Showbox.</p><br><br>";
        $thisDatabaseWriter->db->beginTransaction();

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//  Image Insert into Database
//
        $query = 'INSERT INTO tblImages SET ';

        $query .= 'fldFolder = ?, ';
        $query .= 'fldImage = ?';


        $results = $thisDatabaseWriter->insert($query, $dataGo);

        $dataEntered = $thisDatabaseWriter->db->commit();

        if ($debug)
            print "<p>transaction complete ";
    }else {
        print_r($errors);
    }
}




//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//  Form
//
?>


<form action = "upload.php" method = "POST" enctype = "multipart/form-data">
    <p><h5><b>Folder Name:</b></h5></p>
            <input id="folderTo" maxlength="20" name="folderTo" placeholder="Enter your folder name" tabindex="100"  type="text">
            <br>
            <h5><b>Choose Your Image:</b></h5>
            <input type = "file" name = "image" />
            <input type = "submit"/>
            <br>
            <h5><b>Information About Your Image:</b></h5>
            <ul>
                <li>Sent file: <?php echo $_FILES['image']['name']; ?>
                <li>File size: <?php echo $_FILES['image']['size']; ?>
                <li>File type: <?php echo $_FILES['image']['type'] ?>
            </ul>

        </form>
<?php
include "footer.php";
?>      
    </body>
</html>