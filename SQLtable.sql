Database RTETHER_showbox
Table structure for table tblImages
Column	Type	Null	Default	Links to	Comments
pmkImageID	timestamp	No	CURRENT_TIMESTAMP	
fldFolder	varchar(30)	No		
fldImage	varchar(100)	No		
Dumping data for table tblImages
pmkImageID	fldFolder	fldImage
2017-05-02 18:18:21	KOALASforEVER	dat-koala-doe.jpg
2017-05-02 18:18:33	oatmeal	im-a-dragon-not-a-lizard.jpg
2017-05-02 18:18:54	family	Happy-family-of-four-on-vacation.jpg
2017-05-02 18:20:38	PencilPasts	look-at-me-im-a-tree.jpg
2017-05-02 19:17:02	Nn	20170502_184941.jpg

-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: webdb.uvm.edu
-- Generation Time: May 05, 2017 at 06:43 PM
-- Server version: 5.6.35-81.0-log
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `RTETHER_showbox`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblImages`
--

CREATE TABLE IF NOT EXISTS `tblImages` (
  `pmkImageID` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fldFolder` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `fldImage` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblImages`
--

INSERT INTO `tblImages` (`pmkImageID`, `fldFolder`, `fldImage`) VALUES
('2017-05-02 22:18:21', 'KOALASforEVER', 'dat-koala-doe.jpg'),
('2017-05-02 22:18:33', 'oatmeal', 'im-a-dragon-not-a-lizard.jpg'),
('2017-05-02 22:18:54', 'family', 'Happy-family-of-four-on-vacation.jpg'),
('2017-05-02 22:20:38', 'PencilPasts', 'look-at-me-im-a-tree.jpg'),
('2017-05-02 23:17:02', 'Nn', '20170502_184941.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblImages`
--
ALTER TABLE `tblImages`
  ADD PRIMARY KEY (`pmkImageID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
